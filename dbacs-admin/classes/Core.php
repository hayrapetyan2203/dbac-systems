<?php 
session_start();
require_once('../../vendor/autoload.php');

use Common\Country;
use Common\Schedule;
use Common\Config;
use Common\User;
use Common\Connect;
use Common\Logs;
/**
 * 
 */
class Core
{
	public $database;
	public $password;
	public $errors;
	public $conn;

	function __construct( $database,  $password)
	{
		$this->database = $database;
		$this->password = $password;
	}

	public function connect()
	{
		try {
			$dbConfig = new Config('localDBConfig');

		    $this->conn = Connect::getConnection($dbConfig);
		    // set the PDO error mode to exception
		    $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $this->setSessionVars();

		    //check if user exists...
		    if(User::exists($this->database, $this->password, $this->conn)){
		    	$message = 'User connected to admin dashboard: ' . date('Ymd_His') . ' : User credentials: ' . json_encode($this);
		    	Logs::setLog($message, $this->database);
		    	return true;
		    }

		} catch(PDOException $e) {
			$this->errors = $e->getMessage();
			return false;
		}
	}

	public function setSessionVars()
	{
		$_SESSION['dbconn'] = [
			'database' => $this->database, 
		];
	}

}

if (isset($_POST['ajax'])) 
{
	$database = trim(stripslashes(htmlspecialchars($_POST['database'])));
	$password = trim(stripslashes(htmlspecialchars($_POST['password'])));
	
	if (Country::Allowed($database)) 
	{
		$user = new Core($database, $password);
		if ($user->connect()) {
			$response = ['code' => 200, 'status' => 'success'];
			 echo json_encode($response);
		}else{
			echo json_encode($user->errors);
		}
	}else{
		throw new Exception('error!!!');
	}
}

if(isset($_POST['addUser']))
{
	$role     = trim(stripslashes(htmlspecialchars($_POST['role'])));
	$password = trim(stripslashes(htmlspecialchars($_POST['password'])));
	$database = trim(stripslashes(htmlspecialchars($_POST['database'])));

	User::addUser($role, $password, $database);

	$message = 'ADDED_USER with credentials: ' . json_encode($role) . json_encode($password) . json_encode($database) . ' IN DATABASE ' . $database;
	Logs::setLog($message, $database);

	header('Location: ' . $_SERVER['HTTP_REFERER']);
}

if(isset($_POST['deleteUser']))
{
	$userId = trim(stripslashes(htmlspecialchars($_POST['userId'])));
	$database = trim(stripslashes(htmlspecialchars($_POST['database'])));

	User::deleteUser($userId);

	$message = 'DELETED_USER with ID: ' . json_encode($userId) . ' IN DATABASE ' . $database;
	Logs::setLog($message, $database);

	header('Location: ' . $_SERVER['HTTP_REFERER']);
}

if(isset($_POST['editUser']))
{
	$id = trim(stripslashes(htmlspecialchars($_POST['id'])));
	$password = trim(stripslashes(htmlspecialchars($_POST['password'])));
	$role = trim(stripslashes(htmlspecialchars($_POST['role'])));
	$database = trim(stripslashes(htmlspecialchars($_POST['database'])));

	User::editUser($id, $password, $role);

	$message = 'EDITED_USER with credentials: ' . json_encode($userId) . json_encode($role) . json_encode($password) . ' IN DATABASE ' . $database;
	Logs::setLog($message, $database);
}

if(isset($_POST['country-list']))
{
	$allowed_countries = $_POST['country-select'];
	$database = trim(stripslashes(htmlspecialchars($_POST['database'])));

	Country::setAlowedCountries($allowed_countries, $database);
	
	$message = 'ALLOWED_COUNTRIES_SETTED with data: ' . json_encode($allowed_countries) . ' IN DATABASE ' . $database;
	Logs::setLog($message, $database);

	header('Location: ' . $_SERVER['HTTP_REFERER']);
}

if(isset($_POST['setSchedule']))
{
	foreach ($_POST as $key => $value) {
	    $_POST[$key] = ($value === '') ? '24-00' : trim(stripslashes(htmlspecialchars($value)));
	}

	$monday_to = $_POST['monday_to'] ?? '24-00';
	$monday_from = $_POST['monday_from'] ?? '24-00';
	$tuesday_to = $_POST['tuesday_to'] ?? '24-00';
	$tuesday_from = $_POST['tuesday_from'] ?? '24-00';
	$wednesday_to = $_POST['wednesday_to'] ?? '24-00';
	$wednesday_from = $_POST['wednesday_from'] ?? '24-00';
	$thursday_to = $_POST['thursday_to'] ?? '24-00';
	$thursday_from = $_POST['thursday_from'] ?? '24-00';
	$friday_to = $_POST['friday_to'] ?? '24-00';
	$friday_from = $_POST['friday_from'] ?? '24-00';
	$saturday_to = $_POST['saturday_to'] ?? '24-00';
	$saturday_from = $_POST['saturday_from'] ?? '24-00';
	$sunday_to = $_POST['sunday_to'] ?? '24-00';
	$sunday_from = $_POST['sunday_from'] ?? '24-00';
	$database = $_POST['database'] ?? '24-00';

	$week_schedule = [
		'monday_to' => $monday_to,
		'monday_from' => $monday_from,
		'tuesday_to' => $tuesday_to,
		'tuesday_from' => $tuesday_from,
		'wednesday_to' => $wednesday_to,
		'wednesday_from' => $wednesday_from,
		'thursday_to' => $thursday_to,
		'thursday_from' => $thursday_from,
		'friday_to' => $friday_to, 
		'friday_from' => $friday_from ,
		'saturday_to' => $saturday_to ,
		'saturday_from' => $saturday_from ,
		'sunday_to' => $sunday_to ,
		'sunday_from' => $sunday_from
	];

	Schedule::setSchedule($week_schedule, $database);

	$message = 'WORKING_SCHEDULE SETTED with data: ' . json_encode($week_schedule) . ' IN DATABASE ' . $database;
	Logs::setLog($message, $database);

	header('Location: ' . $_SERVER['HTTP_REFERER']);
}