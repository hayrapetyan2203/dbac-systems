// Get the modal
var countryModal = document.getElementById("countryModal");
var usersModal = document.getElementById("usersModal");
var auditModal = document.getElementById("auditModal");
var scheduleModal = document.getElementById("scheduleModal");

// Get the button that opens the modal
var countryModalbtn = document.getElementById("countryModalbtn");
var usersModalbtn = document.getElementById("usersModalbtn");
var auditModalbtn = document.getElementById("auditModalbtn");
var scheduleModalbtn = document.getElementById("scheduleModalbtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
countryModalbtn.onclick = function() {
  countryModal.style.display = "block";
}
usersModalbtn.onclick = function() {
  usersModal.style.display = "block";
}
auditModalbtn.onclick = function() {
  auditModal.style.display = "block";
}
scheduleModalbtn.onclick = function() {
  scheduleModal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  countryModal.style.display = "none";
  usersModal.style.display = "none";
  auditModal.style.display = "none";
  scheduleModal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == countryModal || event.target == usersModal || event.target == auditModal || event.target == scheduleModal ) {
      countryModal.style.display = "none";
      usersModal.style.display = "none";
      auditModal.style.display = "none";
      scheduleModal.style.display = "none";
  }
}
