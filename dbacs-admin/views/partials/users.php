<?php 
require_once('../../vendor/autoload.php');

use Common\User;
 ?>
<div id="usersModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
      <table class="table table-sm table-dark">
        <thead>
          <tr>
            <th scope="col">id</th>
            <th scope="col">database</th>
            <th scope="col">password</th>
            <th scope="col">role</th>
          </tr>
        </thead>
        <tbody>
          <?php User::listUsers($database); ?>
          <tr>
            <form action="../classes/Core.php" id="addUserForm" method="post">
              <th scope="row">#</th>
            <td><?php echo $database ?></td>
            <td>
              <input type="text" placeholder="password" name="password"> 
              <input type="hidden" name="addUser" value="true"> 
              <input type="hidden" name="database" value="<?php echo $database ?>">
            </td>
            <td><input type="text" placeholder="role" name="role"></td>
            <td>
                <div id="addUserButton" style="cursor: pointer;" onclick="addUser()">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="green" class="bi bi-person-fill-add" viewBox="0 0 16 16">
                      <path d="M12.5 16a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7Zm.5-5v1h1a.5.5 0 0 1 0 1h-1v1a.5.5 0 0 1-1 0v-1h-1a.5.5 0 0 1 0-1h1v-1a.5.5 0 0 1 1 0Zm-2-6a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"/>
                      <path d="M2 13c0 1 1 1 1 1h5.256A4.493 4.493 0 0 1 8 12.5a4.49 4.49 0 0 1 1.544-3.393C9.077 9.038 8.564 9 8 9c-5 0-6 3-6 4Z"/>
                    </svg>
                </div>
            </td>
            </form>
          </tr>
        </tbody>
      </table>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script>
  function addUser() {
    document.getElementById('addUserForm').submit()
  }

  function deleteUser(user){
    document.getElementById('deleteUserForm').submit()
  }

  function editUser(user) {
    let userInfo = user.parentElement.parentElement
    let id = userInfo.cells[0].innerText
    let password = userInfo.cells[2].firstChild.value
    let role = userInfo.cells[3].firstChild.value
    let database = userInfo.cells[1].innerText
    let editUser = true
    let data = { id : id, password : password, role : role, editUser : editUser, database : database}
    var url  = '../classes/Core.php'
    
       $.ajax({
        type: 'POST',
        url: url,
        data: data,
        beforeSend: function() {
            
        },
        success: function(data) {
            let response = JSON.parse(data)
            if(response.status == 200){
              location.reload()
            }
        },
        error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
        complete: function() {
           
        },
        dataType: 'html'
    });
  }
</script>