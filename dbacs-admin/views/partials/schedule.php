<?php 
  require_once('../../vendor/autoload.php');

  use Common\Schedule;
  $schedule = Schedule::getSchedule($database);
?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css">
  <script src="//code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>
  <script>
    $(function() {
        $('.timepicker').timepicker({
            timeFormat: 'HH:mm',
            interval: 15,
            minTime: '00:00',
            maxTime: '23:45',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
    });
  </script>
<div id="scheduleModal" class="modal">
  <div class="modal-content w-50 bg-secondary">
    <span class="close">&times;</span>
    <form action="../classes/Core.php" method="post" >
      <div class="form-group d-flex p-3 m-4 bg-dark text-white rounded justify-content-end">
        <h4>Monday</h4>
        <input type="text" class="timepicker form-control w-25 ml-4" name="monday_to" value="<?php echo $schedule[0]['monday_to']; ?>">
        <input type="text" class="timepicker form-control w-25 ml-4" name="monday_from" value="<?php echo $schedule[0]['monday_from'] ?>">
      </div>
      <div class="form-group d-flex p-3 m-4 bg-dark text-white rounded justify-content-end">
        <h4>Tuesday</h4>
        <input type="text" class="timepicker form-control w-25 ml-4" name="tuesday_to" value="<?php echo $schedule[0]['tuesday_to'] ?>">
        <input type="text" class="timepicker form-control w-25 ml-4" name="tuesday_from" value="<?php echo $schedule[0]['tuesday_from'] ?>">
      </div>            
      <div class="form-group d-flex p-3 m-4 bg-dark text-white rounded justify-content-end">
        <h4>Wednesday</h4>
        <input type="text" class="timepicker form-control w-25 ml-4" name="wednesday_to" value="<?php echo $schedule[0]['wednesday_to'] ?>">
        <input type="text" class="timepicker form-control w-25 ml-4" name="wednesday_from" value="<?php echo $schedule[0]['wednesday_from'] ?>">
      </div>            
      <div class="form-group d-flex p-3 m-4 bg-dark text-white rounded justify-content-end">
        <h4>Thursday</h4>
        <input type="text" class="timepicker form-control w-25 ml-4" name="thursday_to" value="<?php echo $schedule[0]['thursday_to'] ?>">
        <input type="text" class="timepicker form-control w-25 ml-4" name="thursday_from" value="<?php echo $schedule[0]['thursday_from'] ?>">
      </div>            
      <div class="form-group d-flex p-3 m-4 bg-dark text-white rounded justify-content-end">
        <h4>Friday</h4>
        <input type="text" class="timepicker form-control w-25 ml-4" name="friday_to" value="<?php echo $schedule[0]['friday_to'] ?>">
        <input type="text" class="timepicker form-control w-25 ml-4" name="friday_from" value="<?php echo $schedule[0]['friday_from'] ?>">
      </div>            
      <div class="form-group d-flex p-3 m-4 bg-dark text-white rounded justify-content-end">
        <h4>Saturday</h4>
        <input type="text" class="timepicker form-control w-25 ml-4" name="saturday_to" value="<?php echo $schedule[0]['saturday_to'] ?>">
        <input type="text" class="timepicker form-control w-25 ml-4" name="saturday_from" value="<?php echo $schedule[0]['saturday_from'] ?>">
      </div>            
      <div class="form-group d-flex p-3 m-4 bg-dark text-white rounded justify-content-end">
        <h4>Sunday</h4>
        <input type="text" class="timepicker form-control w-25 ml-4" name="sunday_to" value="<?php echo $schedule[0]['sunday_to'] ?>">
        <input type="text" class="timepicker form-control w-25 ml-4" name="sunday_from" value="<?php echo $schedule[0]['sunday_from'] ?>">
      </div>
      <input type="hidden" name="setSchedule" value="true">
      <input type="hidden" name="database" value="<?php echo $database; ?>">
      <div align="center">
          <button class="btn btn-lg w-75 btn-success form controll m-4" type="submit">Save</button>
      </div>
    </form>
  </div>
</div>

