<?php 
  require_once('../../vendor/autoload.php');

  use Common\Country;
  $countryList = Country::ListCounties();
  $allowed_countries = Country::getAllowedCountries($database);
?>
  <style>
    .ms-list{
      width: 200px ;
    }
    .countryModal{
      width: 800px;      
    }
  </style>
 <link href="../../dbacs_common/js/multiselect/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<div id="countryModal" class="modal">
  <div class="modal-content countryModal">
    <span class="close">&times;</span>
    <div align="center">
       <form action="../classes/Core.php" method="post">
        <input type="hidden" name="country-list" value="true">
        <input type="hidden" name="database" value="<?php echo $database ?>">
         <select multiple="multiple" id="country-select" name="country-select[]" style="margin: 0 auto;">
          <?php 
            foreach ($countryList as $countryKey => $countryValue) {
              ?>
                <option value="<?php echo $countryKey?>"> <?php echo $countryValue ?> </option>
              <?php
            }
           ?>
        </select>
        <button type="submit" class="btn btn-success btn-sm mt-5">Save</button>
       </form>
    </div>
    <div id="alowed_countries mt-5" align="center" style="margin-top: 60px!important;">
      <h3>Allowed counties</h3>
      <div align="left">
        <ul>
          <?php 
            foreach ($allowed_countries as $country) {
              echo "<li>" . $country ."</li>";
            }
          ?> 
        </ul>
      </div>
    </div>
    <script src="../../dbacs_common/js/multiselect/js/jquery.multi-select.js" type="text/javascript"></script>

    <script>
    $('#country-select').multiSelect();
    </script>
  </div>
</div>