<?php 
  require_once('../../vendor/autoload.php');

  use Common\Logs;

  $logs = Logs::getLogs($database);
?>
<div id="auditModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <div id="database_logs mt-5" align="center" style="margin-top: 60px!important;">
      <h3>Database logs</h3>
      <div align="left" style="background-color: black; padding: 10px; color: limegreen; border-radius: 10px;">
        <ul>
          <?php 
            foreach ($logs as $log) {
              echo "<li>" . $log['message'] ."</li>";
            }
          ?> 
        </ul>
      </div>
    </div>
  </div>
</div>