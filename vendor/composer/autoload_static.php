<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2575840fa2cc8f91dcfaae0d8cfb7864
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'Common\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Common\\' => 
        array (
            0 => __DIR__ . '/../..' . '/dbacs_common/classes',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2575840fa2cc8f91dcfaae0d8cfb7864::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2575840fa2cc8f91dcfaae0d8cfb7864::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
