<?php 
namespace Common;
/**
 * 
 */
class Config 
{
	const localDBConfig = 'localDBConfig';
	const devDBConfig = 'devDBConfig';
	const prodDBConfig = 'prodDBConfig';

	public $host;
	public $port;
	public $database;
	public $username;
	public $password;

	public function __construct($methodName)
	{
		$this->setDBConfig($methodName);
	}

	public  function setDBConfig($methodName)
	{
		return method_exists($this, $methodName) ? $this->$methodName() : null;
	}

	public function localDBConfig()
	{
		$this->host = 'localhost';
		$this->port = 3306;
		$this->database = 'dacs';
		$this->username = 'root';
		$this->password = '';
	}

	public function devDBConfig()
	{
		// code...
	}

	public function prodDBConfig()
	{
		// code...
	}
}
 ?>