<?php 
namespace Common;
//https://static-maps.yandex.ru/1.x/?lang=en_US&ll=44.51361,40.18111&size=450,450&z=10&l=map&pt=44.51361,40.18111,pm2rdl1~32.870152,39.869847,pm2rdl99
//exec(string COMMAND, array OUTPUT, int RETURN_VARIABLE);

/**
 * 
 */
class Shodan
{
	const BASE_URL = 'https://api.shodan.io';
	
	public static function Search($ip){
		
		$ch = curl_init();
		$url= 'https://api.shodan.io/shodan/host/'. $ip .'?key=pXECP6Vhs3Lcoyg6rvJolTx7AXGuV1qZ';
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
		    echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		return $result;
	}
}