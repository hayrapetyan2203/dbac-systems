<?php 
namespace Common;

use Common\Config;
use Common\Connect;
use PDO;
/**
 * 
 */
class User 
{
	public static $conn;

	public static function exists($database, $password, $conn)
	{
	    // Prepare and execute query to check if user exists
	    try {
	        $stmt = $conn->prepare("SELECT COUNT(*) FROM users WHERE database_name = :database AND password = :password");
	        $stmt->execute(['database' => $database, 'password' => $password]);
	        $count = $stmt->fetchColumn();
	    } catch (PDOException $e) {
	        // Handle the exception as appropriate, e.g. log the error, display a message to the user, etc.
	        echo "An error occurred: " . $e->getMessage();
	        return false;
	    }
	    
	    // Check if user exists based on query result
	    if ($count > 0) {
	        return true;
	    } else {
	        return false;
	    }
	}

	public static function listUsers($database)
	{
		$dbConfig = new Config('localDBConfig');
		self::$conn = Connect::getConnection($dbConfig);

		$stmt = self::$conn->prepare("SELECT * FROM users WHERE database_name = :database_name");
	    $stmt->execute(array(':database_name' => 'laminat'));

	    // Fetch all rows from the query result
	    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

	    // Do something with the rows
	    foreach ($rows as $row) {
	        ?>
		      <tr>
		      	<form action="../classes/Core.php" id="deleteUserForm" method="post">
			      	<th scope="row"><?php echo $row['id']; ?> 
				      	<input type="hidden" name="deleteUser" value="true"> 
				      	<input type="hidden" name="userId" value="<?php echo $row['id']; ?>">
				      	<input type="hidden" name="database" value="<?php echo $row['database_name']; ?>">
			        </th>
		            <td><?php echo $row['database_name']; ?></td>
		           <td><input type="text" name="password" value="<?php echo $row['password']; ?>"></td> 
		           <td><input type="text" name="role" value="<?php echo $row['role']; ?>"></td> 
		            <td id="deleteUser">
		            	<div style="cursor: pointer;" onclick="deleteUser(this)">
		            		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-person-dash" viewBox="0 0 16 16">
								  <path d="M12.5 16a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7ZM11 12h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1 0-1Zm0-7a3 3 0 1 1-6 0 3 3 0 0 1 6 0ZM8 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4Z"/>
								  <path d="M8.256 14a4.474 4.474 0 0 1-.229-1.004H3c.001-.246.154-.986.832-1.664C4.484 10.68 5.711 10 8 10c.26 0 .507.009.74.025.226-.341.496-.65.804-.918C9.077 9.038 8.564 9 8 9c-5 0-6 3-6 4s1 1 1 1h5.256Z"/>
							</svg>
		            	</div>
		            </td>		            
		            <td id="editUser">
		            	<div style="cursor: pointer;" onclick="editUser(this)">
		            		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-pencil-square" viewBox="0 0 16 16">
							  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
							  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
							</svg>
		            	</div>
		            </td>
		      	</form>
	          </tr>
	        <?php
	    }
	}

	public static function addUser($role, $password, $database)
	{
		$dbConfig   = new Config('localDBConfig');
		self::$conn = Connect::getConnection($dbConfig);

		$stmt = self::$conn->prepare("INSERT INTO users(database_name, password, role) VALUES(:database_name, :password, :role) ");
	    return $stmt->execute(array(':database_name' => $database, ':password' => $password, ':role' => $role));
	}

	public static function deleteUser($userId)
	{
		$dbConfig   = new Config('localDBConfig');
		self::$conn = Connect::getConnection($dbConfig);

		$stmt = self::$conn->prepare("DELETE FROM users WHERE id = :userId ");
	    return $stmt->execute(array(':userId' => $userId));
	}

	public static function editUser($id, $password, $role)
	{
		$dbConfig   = new Config('localDBConfig');
		self::$conn = Connect::getConnection($dbConfig);

		try {
			$stmt = self::$conn->prepare("UPDATE users SET password = :password, role = :role WHERE id = :id ");
	        $stmt->execute(array(':id' => $id, ':password' => $password, ':role' => $role));
	        $response =  [ 'status' => 200, 'message' => 'success'];
	        echo json_encode($response);
		} catch (Exception $e) {
			echo json_encode($e->getMessage());
		}
	}
}
 ?>