<?php
namespace Common; 

use PDO;
/**
 * 
 */
class Connect 
{
	
	public static function getConnection($dbConfig)
	{
		return new PDO("mysql:host=$dbConfig->host;dbname=$dbConfig->database;port=$dbConfig->port", $dbConfig->username, $dbConfig->password);
	}
}
 ?>