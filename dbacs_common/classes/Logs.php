<?php 
namespace Common;
require_once('../../vendor/autoload.php');

use Common\Config;
use Common\Connect;
use PDO;
/**
 * 
 */
class Logs 
{
	public static $conn;

	public static function setLog($message, $database) 
	{
	    $dbConfig = new Config('localDBConfig');
	    self::$conn = Connect::getConnection($dbConfig);
	    $stmt = self::$conn->prepare("INSERT INTO database_logs(database_name, message) VALUES(:database_name, :message) ");
	    $stmt->execute(array(':database_name' => $database, ':message' => $message));
	}

	public static function getLogs($database)
	{
		$dbConfig = new Config('localDBConfig');
	    self::$conn = Connect::getConnection($dbConfig);

	    $stmt = self::$conn->prepare("SELECT * FROM database_logs WHERE database_name = :database_name ");
	    $stmt->execute(array(':database_name' => $database));

	    $results = array();
	    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
	        $results[] = $row;
	    }
	    return $results;
	}
}
 ?>