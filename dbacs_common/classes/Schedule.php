<?php 
namespace Common;
require_once('../../vendor/autoload.php');

use Common\Config;
use Common\Connect;
use PDO;
/**
 * 
 */
class Schedule 
{
	public static $conn;

	public static function setSchedule($week_schedule, $database) 
	{
	    $dbConfig = new Config('localDBConfig');
	    self::$conn = Connect::getConnection($dbConfig);

	    if (!empty($week_schedule)) {
	        $sql = self::$conn->prepare("DELETE FROM database_schedule WHERE database_name = :database_name");
	        $sql->execute([':database_name' => $database]);
	    }

	    $columns = implode(', ', array_keys($week_schedule));
		$values = ':' . implode(', :', array_keys($week_schedule));
		$sql = "INSERT INTO database_schedule ($columns, database_name) VALUES ($values, :database_name)";
		$stmt = self::$conn->prepare($sql);	

		// Bind parameters and execute the statement
		foreach ($week_schedule as $key => $value) {
		    $stmt->bindValue(":$key", $value);
		    $stmt->bindValue(":database_name", $database);
		}
		$stmt->execute();
	}

	public static function getSchedule($database) 
	{
	    $dbConfig = new Config('localDBConfig');
	    self::$conn = Connect::getConnection($dbConfig);

	    $sql = self::$conn->prepare("SELECT DISTINCT * FROM database_schedule WHERE database_name = :database_name");
	    $sql->execute([':database_name' => $database]);
	    $result = $sql->fetchAll(PDO::FETCH_ASSOC);
	    if(!isset($result) || empty($result)){	    	
	    	$result =  [
	    		0 => [
	    			'monday_to' => '24-00',
					'monday_from' => '24-00',
					'tuesday_to' => '24-00',
					'tuesday_from' => '24-00',
					'wednesday_to' => '24-00',
					'wednesday_from' => '24-00',
					'thursday_to' => '24-00',
					'thursday_from' => '00-00',
					'friday_to' => '24-00', 
					'friday_from' => '24-00' ,
					'saturday_to' => '24-00' ,
					'saturday_from' => '24-00' ,
					'sunday_to' => '24-00' ,
					'sunday_from' => '24-00'
	    		]
	    	];
	    }

	    return $result;
	}
}
 ?>