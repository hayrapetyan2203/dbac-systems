$("#submit").click(function(){
    let database = $("#database").val()
    let password = $("#password").val()
    let ajax = true
    let data = { database : database, password : password, ajax : ajax}
    let url  = 'classes/Core.php' 

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        beforeSend: function() {
            
        },
        success: function(data) {
            let response = JSON.parse(data)
            console.log(data)
            if (response.code == 200) {
               window.location.replace("../../dbacs-admin/views/dashboard.php");
            }
        },
        error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
        complete: function() {
           
        },
        dataType: 'html'
    });
})