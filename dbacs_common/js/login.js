$("#submit").click(function(){
    let host = $("#host").val()
    let port = $("#port").val()
    let database = $("#database").val()
    let username = $("#username").val()
    let password = $("#password").val()
    let ajax = true
    let data = {host : host, port : port, database : database, username : username, password : password, ajax : ajax}
    let url  = 'classes/Core.php' 

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        beforeSend: function() {
            
        },
        success: function(data) {
            let response = JSON.parse(data)
            if (response.code == 200) {
                window.location.replace("views/database.php");
            }
        },
        error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
        complete: function() {
           
        },
        dataType: 'html'
    });
})