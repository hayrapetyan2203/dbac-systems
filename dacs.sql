-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 13 2023 г., 00:54
-- Версия сервера: 10.4.19-MariaDB
-- Версия PHP: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dacs`
--

-- --------------------------------------------------------

--
-- Структура таблицы `dashboard_users`
--

CREATE TABLE `dashboard_users` (
  `id` int(12) NOT NULL,
  `email` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `database_name` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `dashboard_users`
--

INSERT INTO `dashboard_users` (`id`, `email`, `password`, `database_name`) VALUES
(1, 'hayrapetyan2203@gmail.com', 'hayrapetyan2203', 'dacs'),
(2, 'hayrapetyan2203@gmail.com', 'test', 'dacs'),
(3, '', '', ''),
(4, 'hayrapetyan2201@gmail.com', 'hayrapetyan2201', 'dacs');

-- --------------------------------------------------------

--
-- Структура таблицы `database_country`
--

CREATE TABLE `database_country` (
  `id` int(11) NOT NULL,
  `database_name` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_iso` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `database_country`
--

INSERT INTO `database_country` (`id`, `database_name`, `country_iso`) VALUES
(11, 'laminat', 'AM');

-- --------------------------------------------------------

--
-- Структура таблицы `database_logs`
--

CREATE TABLE `database_logs` (
  `id` int(12) NOT NULL,
  `message` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `database_name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `database_logs`
--

INSERT INTO `database_logs` (`id`, `message`, `database_name`) VALUES
(1, 'User connected to admin dashboard: 20230313_005104 : User credentials: {\"database\":\"laminat\",\"password\":\"laminat\",\"errors\":null,\"conn\":{}}', 'laminat');

-- --------------------------------------------------------

--
-- Структура таблицы `database_schedule`
--

CREATE TABLE `database_schedule` (
  `id` int(12) NOT NULL,
  `database_name` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `monday_to` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `monday_from` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tuesday_to` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tuesday_from` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wednesday_to` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wednesday_from` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thursday_to` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thursday_from` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `friday_to` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `friday_from` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saturday_to` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saturday_from` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sunday_to` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sunday_from` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `database_schedule`
--

INSERT INTO `database_schedule` (`id`, `database_name`, `monday_to`, `monday_from`, `tuesday_to`, `tuesday_from`, `wednesday_to`, `wednesday_from`, `thursday_to`, `thursday_from`, `friday_to`, `friday_from`, `saturday_to`, `saturday_from`, `sunday_to`, `sunday_from`) VALUES
(9, 'laminat', '01:31', '24-00', '24-00', '24-00', '24-00', '24-00', '24-00', '00-00', '24-00', '24-00', '24-00', '24-00', '24-00', '24-00');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(12) NOT NULL,
  `database_name` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `database_name`, `password`, `role`) VALUES
(1, 'laminat', 'laminat', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `dashboard_users`
--
ALTER TABLE `dashboard_users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `database_country`
--
ALTER TABLE `database_country`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `database_logs`
--
ALTER TABLE `database_logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `database_schedule`
--
ALTER TABLE `database_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `dashboard_users`
--
ALTER TABLE `dashboard_users`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `database_country`
--
ALTER TABLE `database_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `database_logs`
--
ALTER TABLE `database_logs`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `database_schedule`
--
ALTER TABLE `database_schedule`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
