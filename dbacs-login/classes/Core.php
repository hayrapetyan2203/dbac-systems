<?php 
session_start();
require_once('../../vendor/autoload.php');

use Common\Country;
/**
 * 
 */
class Core
{
	public $host;
	public $port;
	public $database;
	public $username;
	public $password;
	public $conn;

	function __construct($host, $port, $database, $username, $password)
	{
		$this->host = $host;
		$this->port = $port;
		$this->database = $database;
		$this->username = $username;
		$this->password = $password;
	}

	public function connect()
	{
		try {
		    $this->conn = new PDO("mysql:host=$this->host;dbname=$this->database;port=$this->port", $this->username, $this->password);
		    // set the PDO error mode to exception
		    $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $this->setSessionVars();
		    return true;
		} catch(PDOException $e) {
			$this->errors = $e->getMessage();
			return false;
		}
	}

	public function setSessionVars()
	{
		$_SESSION['dbconn'] = [
			'host' => $this->host, 
			'port' => $this->port, 
			'database' => $this->database, 
			'username' => $this->username, 
			'password' => $this->password
		];
	}

}

if (isset($_POST['ajax'])) 
{
	$host     = trim(stripslashes(htmlspecialchars($_POST['host'])));
	$port 	  = trim(stripslashes(htmlspecialchars($_POST['port'])));
	$database = trim(stripslashes(htmlspecialchars($_POST['database'])));
	$username = trim(stripslashes(htmlspecialchars($_POST['username'])));
	$password = trim(stripslashes(htmlspecialchars($_POST['password'])));
	
	if (Country::Allowed($database)) 
	{
		$user = new Core($host, $port, $database, $username, $password);
		if ($user->connect()) {
			$response = ['code' => 200, 'status' => 'success'];
			echo json_encode($response);
		}else{
			echo json_encode($user->errors);
		}
	}else{
		throw new Exception('error!!!');
	}
	
}